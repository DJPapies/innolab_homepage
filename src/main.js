import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import * as Bootstrap from "bootstrap";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

Vue.config.productionTip = false;
Vue.use(Bootstrap);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
